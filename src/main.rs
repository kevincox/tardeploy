extern crate iron;
extern crate tar;

use std::fmt::Write;

fn upload(
	dir: &std::path::Path,
	name: &str,
	req: &mut iron::Request,
) -> iron::IronResult<iron::Response> {
	if req.method != iron::method::Put {
		let mut res = iron::Response::with((
			iron::status::MethodNotAllowed,
			"Use PUT.\n"));
		res.headers.set(iron::headers::ContentType::plaintext());
		return Ok(res)
	}
	let path = dir.join(name);
	if let Err(e) =  std::fs::create_dir(&path) {
		return Ok(iron::Response::with(match e.kind() {
			std::io::ErrorKind::AlreadyExists => (
				iron::status::Conflict,
				"Deploy directory already exists.\n".into()),
			_ => (
				iron::status::InternalServerError,
				format!("Error creating deploy directory: {:?}\n", e)),
		}))
	}

	let mut warnings = "Archive uploaded. Any warnings will follow.\n".to_string();

	let mut archive = tar::Archive::new(&mut req.body);
	let entries = match archive.entries() {
		Ok(e) => e,
		Err(e) => return Ok(iron::Response::with((
				iron::status::BadRequest, format!("Error reading archive: {:?}\n", e)))),
	};
	for entry in entries {
		let mut entry = match entry {
			Ok(e) => e,
			Err(e) => {
				write!(warnings, "- Couldn't read entry: {}\n", e).unwrap();
				continue
			}
		};
		if entry.header().entry_type() != tar::EntryType::file() {
			continue
		}

		if let Err(e) = entry.unpack_in(&path) {
			write!(warnings, "- Couldn't extract: {}\n", e).unwrap();
		}
	}
	let mut res = iron::Response::with((
		iron::status::Ok,
		warnings));
	res.headers.set(iron::headers::ContentType::plaintext());
	Ok(res)
}

fn main() {
	let root = std::path::Path::new("uploads");
	std::fs::create_dir_all(&root).expect("Could not create root");

	iron::Iron::new(move |req: &mut iron::Request| {
		let url = req.url.clone();
		match url.path().as_slice() {
			&["upload", name] => upload(&root, &name, req),
			_ => {
				let mut res = iron::Response::with((
					iron::status::NotFound,
					"Not found\n"));
				res.headers.set(iron::headers::ContentType::plaintext());
				Ok(res)
			}
		}
	}).http("localhost:3000").unwrap();
}
