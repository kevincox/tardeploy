with import <nixpkgs> {}; rec {
	out = rustPlatform.buildRustPackage {
		name = "tardeploy";
		cargoSha256 = null;
		src = builtins.filterSource (name: type:
			(lib.hasPrefix (toString ./src) name) ||
			(lib.hasPrefix (toString ./Cargo) name)) ./.;
	};
}
